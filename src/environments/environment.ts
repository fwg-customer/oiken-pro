import {Environment} from './environment.interface';
import {environment as local} from './environment.local';
import {environment as prod} from './environment.prod';
export const environment = {
  production: false
};

