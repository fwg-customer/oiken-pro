export interface Environment {
    production: boolean;  
    apis: {
        base: string;
        get_services: string;
    };
}
