import {Environment} from './environment.interface';

export const environment: Environment = {
    production: false,    
    apis: {
        base: 'oiken-pro_api',
        get_services: '/getServices'
    }
};
