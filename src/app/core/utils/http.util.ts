import {HttpErrorResponse, HttpParams} from '@angular/common/http';

import {MessageException, RequestException} from '../entities/error.entity';
import {CommonUtil} from './common.util';

export class HttpUtil {
    static toHttpParams(obj: object): HttpParams {
        let params = new HttpParams();
        for (const key in obj) {
            if (obj.hasOwnProperty(key)) {
                const val = obj[key];
                if (CommonUtil.isNotBlank(val))
                    params = params.append(key, CommonUtil.toString(val));
            }
        }
        return params;
    }

    static parseErrors(value: any): Error {
        const error: any = value.error;
        if (CommonUtil.isNotBlank(error)) {
            if (typeof error.type === 'string') {
                if (error.type === 'invalid')
                    return new MessageException(error.message, error.type, error.errors);
                if (error.type === 'logout')
                    return new RequestException(error.message, 43, error.errors);
                if (error.type.startsWith('notification_'))
                    return new MessageException(error.message, error.type.replace('notification_', ''), error.errors);
            }
            return new RequestException(error.message, value.status, error.errors);
        }
        return undefined;
    }

    static parseResponse(value: any): any {
        /*if (value) {
            if (value.status == null && (value.error != null || value.errors != null || value.content == null))
                throw new RequestException('Does not have status property in the response.');
            if (value.status === false)
                throw HttpUtil.parseErrors(value) || new RequestException();
        }
        return value.content;*/
        return value;
    }

    static toErrorResponse(error: HttpErrorResponse): Error {
        let result: Error;
        if (error != null) {
            if (typeof error.status === 'number') {
                if (typeof error.error === 'object')
                    result = HttpUtil.parseErrors(error);
                if (result == null) {
                    switch (error.status) {
                        case 400:
                            result = new RequestException('Bad Request', 400);
                            break;
                        case 401:
                            result = new RequestException('Token Invalid', 401);
                            break;
                        case 403:
                            result = new RequestException('Unauthorized Access', 403);
                            break;
                        case 404:
                            result = new RequestException('The requested API endpoint does not exist', 404);
                            break;
                        case 500:
                            result = new RequestException(
                                'An Unexpected error has occurred. Please wait a minute and try again.', 500);
                            break;
                        case 504:
                            result = new RequestException('Server Connection Lost', 504);
                            break;
                        default:
                            result = new RequestException(error.statusText || error.message, error.status);
                            break;
                    }
                }
            } else result = new RequestException();
        }
        return result;
    }
}
