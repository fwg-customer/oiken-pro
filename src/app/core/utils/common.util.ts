export class CommonUtil {
    public static isBlank(value: any): boolean {
        return value === null || value === undefined || (value.length !== undefined && value.length === 0);
    }

    public static isNotBlank(value: any): boolean {
        return value !== null && value !== undefined && (value.length === undefined || value.length > 0);
    }

    public static toString(value: any): string {
        if (value === null || value === undefined || value.constructor === String) {
            return value;
        } else if (value instanceof Date) {
            return value.toJSON();
        } else {
            try {
                return JSON.stringify(value);
            } catch (e) {
                return value + '';
            }
        }
    }

    private constructor() {
    }
}
