import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import { RequestException } from '../entities/error.entity';
import { HttpUtil } from '../utils/http.util';



@Injectable({
    providedIn: 'root'
})
export class HttpService {
    private nativeHttp: HttpService;
    public token: string;

    constructor(private http: HttpClient) {
    }

  

    protected buildHeaders(useAuth: boolean, headers?: HttpHeaders): HttpHeaders {
        if (!(headers instanceof HttpHeaders)) {
            headers = new HttpHeaders();
        }
        
        if (!headers.has('Accept')) {
            if (headers.get('Content-Type') === 'text/plain') {
                headers = headers.set('Accept', 'text/plain');
            } else {
                headers = headers.set('Accept', 'application/json');
            }
        }

        return headers;
    }

    
    get<T>(
        url: string,
        useAuth: boolean,
        data?: any,
        headers?: HttpHeaders,
        responseType?: string
    ): Observable<T> {
        const options = {           
            headers: this.buildHeaders(useAuth, headers)
        };

        if (responseType) {
            options['responseType'] = responseType;
        }

        return this.http.get<T>(url, options).pipe(this.handleError);
    }

    post<T>(
        url: string,
        useAuth: boolean,
        data?: any,
        headers?: HttpHeaders,
        responseType?: string
    ): Observable<T> {
        const options = {
            headers: this.buildHeaders(useAuth, headers)
        };

        if (responseType) {
            options['responseType'] = responseType;
        }

        return this.http.post<T>(url, data, options).pipe(this.handleError);
    }

    put<T>(
        url: string,
        useAuth: boolean,
        data?: any,
        headers?: HttpHeaders,
        responseType?: string
    ): Observable<T> {
        const options = {
            headers: this.buildHeaders(useAuth, headers)
        };

        if (responseType) {
            options['responseType'] = responseType;
        }
        if (data && data.skipHandleError) {
            return this.http.put<T>(url, options);
        }
        return this.http.put<T>(url, data, options).pipe(this.handleError);
    }

    delete<T>(
        url: string,
        useAuth: boolean,
        data?: any,
        headers?: HttpHeaders
    ): Observable<T> {
        const options = {           
            headers: this.buildHeaders(useAuth, headers)
        };

        return this.http.delete<T>(url, options).pipe(this.handleError);
    }
    protected handleError(source: Observable<any>) {
        return source.pipe(
            map(value => HttpUtil.parseResponse(value)),
            catchError(err => {
                if (err instanceof HttpErrorResponse) {
                    throw HttpUtil.toErrorResponse(err);
                }
                throw new RequestException(err.message);
            })
        );
    }

}
