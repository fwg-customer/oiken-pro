import {MappingError} from 'data-mapping';

export interface RequestError {
    code?: number;
    message: string | string[];
}

export class MessageException extends MappingError {
    public type: string;
    public data: any;

    constructor(message?: string | string[] | RequestError, type = 'error', data?: any) {
        if (typeof message === 'string' || message instanceof Array)
            super(message);
        else if (message && message['message']) {
            const v: RequestError = message;
            super(v.message, v.code);
        } else super();
        this.name = 'MessageException';
        this.data = data;
    }
}

export class RequestException extends MessageException {
    public data: any;

    constructor(message?: string | string[] | RequestError, code?: number, data?: any) {
        super(message);
        this.name = 'RequestException';
        if (code != null) {
            this.code = code;
        } else if (message && message['code'] && typeof message['code'] === 'number') {
            this.code = message['code'];
        }
        this.data = data;
    }
}
