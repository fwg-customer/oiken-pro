const fs = require('fs');
const path = require('path');
const jsonServer = require('json-server');
const server = jsonServer.create();
const middlewares = jsonServer.defaults();
const services = require('./json/services');


server.use(middlewares);
server.use(jsonServer.bodyParser);

server.use('/getServices', (req, res) => {
    res.jsonp(services);
});
server.listen(3010, () => {
  console.log('Mock Server is running');
});

